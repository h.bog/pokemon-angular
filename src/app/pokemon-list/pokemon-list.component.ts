import { Component, OnInit } from '@angular/core';
import { PokeServiceService } from '../poke-service.service'

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit { 

  constructor(private pokeService: PokeServiceService) { }

  pokemons = [];
  nextUrl = '';



  ngOnInit() {

    this.pokeService.sendGetRequest("https://pokeapi.co/api/v2/pokemon/?limit=40").subscribe((resp: any)=>{
      this.nextUrl= resp.next;
      resp.results.forEach(item => {
        this.pokeService.sendGetRequest(item.url).subscribe((resp: any)=>{
          this.pokemons.push(resp)
        })
      })
    })
  }

  loadMorePokeClick(event) {
    this.pokeService.sendGetRequest(this.nextUrl).subscribe((resp: any)=>{
      this.nextUrl = resp.next;
      resp.results.forEach(item =>{
        this.pokeService.sendGetRequest(item.url).subscribe((resp: any)=>{
          this.pokemons.push(resp)
        })
      })
    })
  }

}