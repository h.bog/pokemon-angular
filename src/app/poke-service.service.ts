import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PokeServiceService {

  constructor( private httpClient: HttpClient ) { }

  public sendGetRequest(POKEMON_API){
    return this.httpClient.get(POKEMON_API);
  }
}