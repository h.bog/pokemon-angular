import { Component, OnInit } from '@angular/core';
import { PokeServiceService } from '../poke-service.service'

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {

  id = '';
  url = '';

  constructor(private pokeService: PokeServiceService) { }

  pokemon = {};

  ngOnInit() {
    this.id = window.location.href.split('/')[4];

    this.url = `https://pokeapi.co/api/v2/pokemon/${this.id}`

    this.pokeService.sendGetRequest(this.url).subscribe((resp: any)=>{
      this.pokemon = resp;
    });

    } 
}
